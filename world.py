from BasicVector import Vec2, Vec3, Vec4
import random
import noise


class World:

    def __init__(self, windowSize, pointsRange):
        self.step = Vec2(windowSize.w/pointsRange.x, windowSize.h/pointsRange.y)
        self.pointsRange = pointsRange
        self.points = [[random.random() for x in range(pointsRange.x)] for y in range(pointsRange.y)]

    def getPoints(self):
        return self.points

    def update(self, zOffset):
        self.points = [[(noise.pnoise3(x * self.step.x, y * self.step.y, zOffset)+1)/2 for x in range(self.pointsRange.x)] for y in range(self.pointsRange.y)]

    def __str__(self) -> str:
        return str(self.points)


